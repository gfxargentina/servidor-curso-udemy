import express from 'express';
//este modulo importamos cuando trabajamos haciendo pruebas en graphql
//import graphqlHTTP from 'express-graphql';

//estos modulos utilizamos cuando instalamos apollo-server-express
import { ApolloServer } from 'apollo-server-express';
import { typeDefs } from './data/schema';
import { resolvers } from './data/resolvers';


const app = express();
const server = new ApolloServer({typeDefs, resolvers});

server.applyMiddleware({app});

app.listen({port:4000}, () => console.log(`El Servidor Apollo-Server-Express esta corriendo ${server.graphqlPath} `) );





//UTILIZAMOS ESTO CUANDO ESTAMOS EN MODO PRUEBA DE LAS QUERYS, MUTATIONS DE GRAPHQL

//app.get('/', (req, res) =>{
//    res.send('El Servidor esta Listo y Funcionando, ponte a Codear')
//});


//app.use('/graphql', graphqlHTTP({
    //aqui le agregamos el schema que va a utilizar
  //  schema,
    //el resolver se pasa como rootValue
   // rootValue: root,
    //utilizar graphql
    //graphiql: true
//}));

//app.listen(8000, () => {
  //  console.log('El Servidor esta Funcionando')
//})
