//import {resolvers} from './resolvers';
import {importSchema} from 'graphql-import';
//import {makeExecutableSchema} from 'graphql-tools';


const typeDefs = importSchema('data/schema.graphql');

//utilizamos esto solo cuando estamos usando graphql en modo prueba
//const schema = makeExecutableSchema({typeDefs, resolvers});

export { typeDefs };